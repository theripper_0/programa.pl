<?php

namespace App\Controller;

use App\Utils\ComparisonHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ComparisonController extends AbstractController
{
    /**
     * @Route("/comparison", name="comparison")
     */
    public function index()
    {
        try {
            $cmpClass = (new ComparisonHandler(1, 30, 20, 22, 31, 1983, 12, 8, 2))
                ->addArgument(15)
                ->addArgument(19);

            $msg = $cmpClass->compareArrays();

        } catch (\Exception $e) {
            $msg = $e->getMessage();
        }

        return $this->render('comparison/index.html.twig', [
            'controller_name' => 'ComparisonController',
            'result' => $msg
        ]);
    }
}
