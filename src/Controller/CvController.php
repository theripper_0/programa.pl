<?php

namespace App\Controller;

use App\DTO\DTO;
use App\DTO\PersonDTO;
use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CvController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator)
    {
        $this->em = $em;
        $this->validator = $validator;
    }

    /**
     * @Route("/cv", name="cv")
     */
    public function index()
    {
        return $this->render('cv/index.html.twig', [
            'controller_name' => 'CvController',
        ]);
    }

    /**
     * @Route("/person", name="person")
     */
    public function postPerson(Request $request) {
        if ($request->isXmlHttpRequest() && strtoupper($request->getMethod()) === 'POST') {
            $data = $request->request->all();

            $personDTO = DTO::arrayToObject($data ?? [], new PersonDTO());
            $validation = $this->validator->validate($personDTO);

            if ($validation->count() === 0) {
                $person = DTO::objectToObject($personDTO, Person::class);

                $this->em->persist($person);
                $this->em->flush();

                return new JsonResponse('success', Response::HTTP_CREATED);
            } else {
                return new JsonResponse($validation, Response::HTTP_BAD_REQUEST);
            }
        } else throw new MethodNotAllowedHttpException(['POST']);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getCategories() {
        $stmt = $this->em->getConnection()
            ->prepare('SELECT * FROM menu');
        $stmt->execute();
        $result = $stmt->fetchAll();
        $categories = $this->buildCategoryTree($result);

        return $this->render(
            'cv/categories.html.twig',
            array('categories' => $categories)
        );
    }

    /**
     * @param array $flatArr
     * @param array $nodes
     * @return array
     */
    private function buildCategoryTree(array $flatArr, array &$nodes = []) {
        $arrCol = array_column($flatArr, 'id');
        if (count($arrCol) > 0) {
            $leaf = max($arrCol);
            $lIndex = array_search($leaf, $arrCol);
            $leaf = array_values(array_slice($flatArr, $lIndex, 1, true));
            $leaf = $leaf[0];

            //check if current leaf is not a parent of node
            foreach ($nodes as $k => $node) {
                if ($leaf['id'] === $node['parent_id']) {
                    if (!isset($leaf['sub'])) $leaf['sub'] = [];
                    // i used workaround array splice instead of slice
                    // because there was a problem with preserve keys and unset($nodes[$k]) in the loop
                    $pNode = array_values(array_splice($nodes, $k, 1, false));
                    array_push($leaf['sub'], $pNode[0]);
                } else continue;

                $leaf['sub'] = array_reverse($leaf['sub']);
            }

            $nodes[] = $leaf;
            unset($flatArr[$lIndex]);
            $this->buildCategoryTree($flatArr, $nodes);
        }

        foreach ($nodes as $k => $node)  if ($node === false) unset($nodes[$k]);

        return array_values($nodes);
    }
}
