<?php
/**
 * Created by PhpStorm.
 * User: kamilbanaszkiewicz
 * Date: 23.08.2018
 * Time: 19:21
 */

namespace App\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class PersonDTO extends DTO
{
    const MALE = 1;
    const FEMALE = 0;
    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=1, max=255)
     */
    public $name;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=1, max=255)
     */
    public $surname;

    /**
     * @Assert\Range(min=1, max=99)
     * @Assert\Type(type="integer")
     */
    public $age;

    /**
     * @Assert\Type(type="bool")
     */
    public $gender;

    /**
     * @Assert\Type(type="bool")
     */
    public $accept;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return PersonDTO
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     * @return PersonDTO
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     * @return PersonDTO
     */
    public function setAge($age)
    {
        if (is_numeric($age)) {
            $this->age = (int) $age;
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     * @return PersonDTO
     */
    public function setGender($gender)
    {
        if (is_numeric($gender)) {
            $this->gender = (bool) $gender;
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAccept()
    {
        return $this->accept;
    }

    /**
     * @param mixed $accept
     * @return PersonDTO
     */
    public function setAccept($accept)
    {
        if (!empty($accept)) {
            $this->accept = (bool) $accept;
        }
        return $this;
    }



}