<?php
/**
 * Created by PhpStorm.
 * User: kamilbanaszkiewicz
 * Date: 22.08.2018
 * Time: 16:59
 */

namespace App\DTO;


class DTO
{
    public static function arrayToObject(array $array, DTO $dto) {
        foreach ($array as $propName => $value) {
            $setter = "set${propName}";
            if (method_exists($dto, $setter)) {
                $dto->$setter($value);
            } else continue;
        }

        return $dto;
    }

    public static function objectToObject(DTO $instance, $className) {
        return unserialize(sprintf(
            'O:%d:"%s"%s',
            strlen($className),
            $className,
            strstr(strstr(serialize($instance), '"'), ':')
        ));
    }
}