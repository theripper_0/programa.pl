<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateTableMenuCommand extends Command
{
    protected static $defaultName = 'app:create-table:menu';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em, $name = null)
    {
        parent::__construct($name);
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setDescription('Creates Menu Table');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $stmt = "CREATE TABLE IF NOT EXISTS `menu` (
                  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
                  `parent_id` int(10) unsigned DEFAULT NULL,
                  `name` VARCHAR (255) UNIQUE NOT NULL,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `id_UNIQUE` (`id`),
                  FOREIGN KEY (parent_id)
                  REFERENCES menu(id)
                  ON DELETE CASCADE
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;";

        $query = $this->em->getConnection()->prepare($stmt);
        $query->execute();

        $io->success('You have created menu table, make some use of it.');
    }
}
