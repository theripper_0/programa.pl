<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PopulateTableMenuCommand extends Command
{
    protected static $defaultName = 'app:populate-table:menu';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em, $name = null)
    {
        parent::__construct($name);
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $stmt = "
                 INSERT INTO menu (`parent_id`, `name`) VALUES (null, 'Primary Category');
                 SET @primary:= LAST_INSERT_ID();
                 INSERT INTO menu (`parent_id`, `name`) VALUES (@primary, 'Subcategory A');
                 SET @subA:= LAST_INSERT_ID();
                 INSERT INTO menu (`parent_id`, `name`) VALUES (@primary, 'Subcategory B');
                 SET @subB:= LAST_INSERT_ID();
                 INSERT INTO 
                    menu (`parent_id`, name) 
                 VALUES 
                    (@subA, 'Product A'), 
                    (@subA, 'Product B'),
                    (@subB, 'Product C'),
                    (@subB, 'Product D');
                ";

        $query = $this->em->getConnection()->prepare($stmt);
        $query->execute();

        $io->success('You have populated Menu Table with some example data.');
    }
}
