<?php
/**
 * Created by PhpStorm.
 * User: kamilbanaszkiewicz
 * Date: 20.08.2018
 * Time: 19:12
 */

namespace App\Utils;


interface ComparisonInterface
{
    /**
     * ComparisonInterface constructor.
     * @param int|null ...$numbers
     */
    public function __construct(?int ...$numbers);

    /**
     * @param int $number
     */
    public function addArgument(int $number);

    /**
     * @return bool
     */
    public function compareArrays(): int;
}