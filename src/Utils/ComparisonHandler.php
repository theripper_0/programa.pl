<?php
/**
 * Created by PhpStorm.
 * User: kamilbanaszkiewicz
 * Date: 20.08.2018
 * Time: 19:08
 */

namespace App\Utils;


class ComparisonHandler implements ComparisonInterface
{
    const EVENS_MULTIPLIER = 3;
    const ODDS_MULTIPLIER = 2;
    private $odds = [];
    private $evens = [];

    /**
     * ComparisonHandler constructor.
     * @param int|null ...$numbers
     * @throws \Exception
     */
    public function __construct(?int ...$numbers)
    {
        foreach ($numbers as $number) $this->addArgument($number);
    }

    /**
     * @param int $number
     * @return ComparisonHandler
     * @throws \Exception
     */
    public function addArgument(int $number): self
    {
        if ($this->isNegative($number)) throw new \Exception('Method accepts only positive integers');

        if ($this->isEven($number)) $this->evens[] = $number;
        else $this->odds[] = $number;

        return $this;
    }

    /**
     *
     * @return int
     * 1 if odds are greater, 0 if operands are equal, -1 if evens are greater
     */
    public function compareArrays(): int
    {
        return (array_sum($this->odds) * self::ODDS_MULTIPLIER) <=> (array_sum($this->evens) * self::EVENS_MULTIPLIER);
    }

    /**
     * @param int $number
     * @return bool
     */
    private function isEven(int $number): bool
    {
        return $number % 2 == 0;
    }

    /**
     * @param int $number
     * @return bool
     */
    private function isNegative(int $number): bool
    {
        return $number <= 0;
    }
}