<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countAvailable()
    {
         return $this->createQueryBuilder('p')
            ->select('COUNT(p)')
            ->where('p.available = true')
            ->getQuery()
            ->getSingleScalarResult();

        /**
         * can be also called by method ->count() but only in versions greater than 2.6
         *  return $this->count($criteria); $criteria ['available' => true]
         */

    }

    /**
     * @return ArrayCollection
     */
    public function findAllByUnavailability(): ArrayCollection
    {
        return $this->createQueryBuilder('p')
            ->where('p.available = false')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $phrase
     * @return ArrayCollection
     */
    public function findAllByName(string $phrase): ArrayCollection
    {
        return $this->createQueryBuilder('p')
            ->where('p.name LIKE :phrase')
            ->setParameter('phrase', '%'.$phrase.'%')
            ->getQuery()
            ->getResult();
    }
}
