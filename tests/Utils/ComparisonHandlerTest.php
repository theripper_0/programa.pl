<?php

namespace App\Tests\Utils;

use App\Tests\CustomTestCase;
use App\Utils\ComparisonHandler;

class ComparisonHandlerTest extends CustomTestCase
{
    /**
     * @var ComparisonHandler
     */
    public $ch;

    /**
     * @throws \Exception
     */
    function setUp()
    {
        parent::setUp();
        $this->ch = new ComparisonHandler(1,2,3);
    }

    /**
     * @throws \ReflectionException
     */
    public function testIsNegative()
    {
        $this->assertInternalType('bool', $this->invokeMethod($this->ch, 'isNegative', [1]));
        $this->assertTrue($this->invokeMethod($this->ch, 'isNegative', [-1]));
        $this->assertFalse( $this->invokeMethod($this->ch, 'isNegative', [1]));
        $this->assertTrue( $this->invokeMethod($this->ch, 'isNegative', [0]));
    }

    /**
     * @throws \ReflectionException
     */
    public function testIsEven()
    {
        $this->assertInternalType('bool', $this->invokeMethod($this->ch, 'isEven', [2]));
        $this->assertTrue($this->invokeMethod($this->ch, 'isEven', [2]));
        $this->assertFalse( $this->invokeMethod($this->ch, 'isEven', [1]));
        $this->assertTrue( $this->invokeMethod($this->ch, 'isEven', [1982]));
        $this->assertFalse( $this->invokeMethod($this->ch, 'isEven', [1983]));
        $this->assertTrue( $this->invokeMethod($this->ch, 'isEven', [-2]));
        $this->assertTrue( $this->invokeMethod($this->ch, 'isEven', [0]));
    }

    /**
     * @throws \ReflectionException
     */
    public function testIsNegativeTypeError() {
        $this->expectException(\TypeError::class);

        $this->invokeMethod($this->ch, 'isNegative', ['test']);
    }

    /**
     * @throws \ReflectionException
     */
    public function testIsEvenTypeError() {
        $this->expectException(\TypeError::class);

        $this->invokeMethod($this->ch, 'isEven', ['test']);
    }

    /**
     * @throws \Exception
     */
    public function testAddArgumentNegativeIntException() {
        $this->expectException(\Exception::class);

        $this->ch->addArgument(-1);
    }

    /**
     * @throws \Exception
     */
    public function testAddArgumentTypeError() {
        $this->expectException(\TypeError::class);

        $this->ch->addArgument('test');
    }

    /**
     * @throws \Exception
     */
    public function testAddArgument()
    {
        $ch1 =  $this->ch->addArgument(1);
        $ch2 =  $this->ch->addArgument(2);
       $this->assertInstanceOf(ComparisonHandler::class, $ch1);
       $this->assertAttributeContains(1, 'odds', $ch1);
       $this->assertAttributeContains(2, 'evens', $ch2);
       // setUp has initiated instance with some integers 2 odds and 1 even so after adding this values count should be as below
       $this->assertAttributeCount(2, 'evens', $ch2);
       $this->assertAttributeCount(3, 'odds', $ch2);


    }

    /**
     * @throws \Exception
     */
    public function testCompareArrays()
    {
        //initial setup is 1,2,3 so odds should be greater -> method should result 1
        // 8
        $oddsSum = (1+3) * ComparisonHandler::ODDS_MULTIPLIER;
        //6
        $evensSum = 2 * ComparisonHandler::EVENS_MULTIPLIER;
        $this->assertEquals(8, $oddsSum);
        $this->assertEquals(6, $evensSum);
        // value of odds should be greter so 1
        $this->assertEquals(1, $this->ch->compareArrays());
        //now evens should be greater so -1
        $this->assertEquals(-1, $this->ch->addArgument(2)->compareArrays());
        // i think only in this case evens and odds can be equal
        $this->assertEquals(0, (new ComparisonHandler())->compareArrays());
        $this->assertInternalType('integer', $this->ch->compareArrays());
    }

    /**
     * @param ComparisonHandler $instance
     * @param $property
     * @return mixed
     * @throws \ReflectionException
     */
    private function getPrivatePropertyValue(ComparisonHandler $instance, $property)
    {
        $reflector = new \ReflectionClass($instance);
        $reflectorProperty = $reflector->getProperty($property);
        $reflectorProperty->setAccessible(true);

        return $reflectorProperty->getValue($instance);
    }
}