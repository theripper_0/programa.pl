<?php
/**
 * Created by PhpStorm.
 * User: kamil
 * Date: 22.07.2018
 * Time: 22:28
 */
namespace App\Tests;

use PHPUnit\Framework\TestCase;

class CustomTestCase extends TestCase
{

    /**
     * @param $object
     * @param $methodName
     * @param array $parameters
     * @return mixed
     * @throws \ReflectionException
     */
    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
