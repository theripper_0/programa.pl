To get use of this project you need to:

0. set .env 
1. composer install ofcourse 
2. yarn install // if there will be any problems with assets
3. php bin/console d:s:u -f
4. php bin/console app:create-table:menu  - which creates Menu Table
5. php bin/console app:populate-table:menu  - which populate above table with data.


I added extra features such xhr submission of form, and some unit tests.
jQuery validation and css is under corresponding dirs in assets dir,
PHP code under /tests and /src
available routes -> '/cv' && '/comparison'

PURE SQL STATEMENTS:
CREATE TABLE IF NOT EXISTS `menu` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `parent_id` int(10) unsigned DEFAULT NULL,
          `name` VARCHAR (255) UNIQUE NOT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `id_UNIQUE` (`id`),
          FOREIGN KEY (parent_id)
          REFERENCES menu(id)
          ON DELETE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
                
INSERT INTO menu (`parent_id`, `name`) VALUES (null, 'Primary Category');
         SET @primary:= LAST_INSERT_ID();
         INSERT INTO menu (`parent_id`, `name`) VALUES (@primary, 'Subcategory A');
         SET @subA:= LAST_INSERT_ID();
         INSERT INTO menu (`parent_id`, `name`) VALUES (@primary, 'Subcategory B');
         SET @subB:= LAST_INSERT_ID();
         INSERT INTO 
            menu (`parent_id`, name) 
         VALUES 
            (@subA, 'Product A'), 
            (@subA, 'Product B'),
            (@subB, 'Product C'),
            (@subB, 'Product D');

I think, thats all :) 