/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../../node_modules/mdbootstrap/css/bootstrap.min.css');
require('../../node_modules/mdbootstrap/css/mdb.min.css');
require('../css/app.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
var $ = require('jquery');
global.$ = global.jQuery = $;

require('popper.js/dist/umd/popper');
require('../../node_modules/mdbootstrap/js/bootstrap.min.js');
require('../../node_modules/mdbootstrap/js/mdb.min.js');

console.log('Hello Webpack Encore! Edit me in assets/js/app.js');



const ValidatorModule = (function () {

    const validateRequired = function (input) {
        $(input).on('change blur keyup', function (e) {
            e.preventDefault();
            const val = this.value;
            const span = $(this).next('span');
            if ((val === '' || val === 'on') && !$(this).is(':checked')) {
                const label = $(this).prev('label').text();
                const msg = `<span class="form-error">Field ${label} is required</span>`;
                if (span.length === 0) {
                    $(this).after(msg);
                    return false;
                }
            } else {
                $(span).remove();
                return true;
            }
        });
        // return false;
    };

    const validateRange = function (input, min, max) {
            const val = parseInt(isNaN(input.value) ? input.val() : input.value);
            console.log(input, val);
            const span = $('#age-error');
            if (!(val >= min && val <= max)) {
                const label = $(input).prev('label').text();
                const msg = `<span id="age-error" class="form-error">${label} should be between 18 and 99 years</span>`;
                if (span.length === 0) {
                    $(input).after(msg);
                }
                return false;
            }
            else {
                $(span).remove();
                return true
            }
    };

    const validateRadios = function (input) {
        const gender = $(input);
        const span = $('#gender-err');
        $(span).remove();
        const inputs = $('input[type=radio]');
        if (!inputs[0].checked && !inputs[1].checked) {
            $(gender).append('<span id="gender-err" class="form-error">Gender must be checked</span>');
            return false
        }
        for(const input of inputs) {
            $(input).on('focus click', function () {
                $('#gender-err').remove();
                return true
            })
        }
    };

    const validateOnSubmit = function(inputs) {
        let flag = true;
        $(inputs).each(function() {
            const span = $(this).next('span');
            const val = this.value;
            if((val === '' || val === 'on') && !$(this).is(':checked')) {
                const label = $(this).prev('label').text();
                const msg = `<span class="form-error">Field ${label} is required</span>`;
                if (span.length === 0) {
                    $(this).after(msg);
                }
                flag = false
            }
        });
        const gender = $('#gender');
        const span = $('#gender-err');
        $(span).remove();
        const radios = $('input[type=radio]');
        if (!radios[0].checked && !radios[1].checked) {
            $(gender).append('<span id="gender-err" class="form-error">Gender must be checked</span>');
        }

        const age = $('#age');
        const range = validateRange(age, 18, 99);

       console.log(flag, (radios[0].checked || radios[1].checked), range);
       return Boolean(flag & (radios[0].checked || radios[1].checked) & range);

    };

    return {
        validateRequired: validateRequired,
        validateRange: validateRange,
        validateOnSubmit: validateOnSubmit,
        validateRadios: validateRadios
    }

})();



(function () {

    let scrollPosition;
    let target;

    $(function() {
        $('a[href*="#"]').on('click', function(e) {
            e.preventDefault();
            target = $(this).attr("href");
            if (target === '#') {
                target = '#cv';
            }
            scrollPosition = $(target).offset().top - 100; // section header must be visible because of fixed nav
            $('html, body').stop().animate({
                scrollTop: scrollPosition
            }, 600, function(e) {
                history.pushState(null, null, target);
            });

            return false;
        });
    });

    $(window).scroll(function() {
        const scrollDistance = $(window).scrollTop();
        $('section').each(function() {
            if (scrollPosition != scrollDistance) {
                $('.navbar-nav li.active').removeClass('active');
                $(`.navbar-nav a[href="${target}"]`).closest('li').addClass('active');
            }
        });
    }).scroll();

    $('.dropdown-submenu .dropdown-toggle').on("click", function(e) {
        e.stopPropagation();
        e.preventDefault();
        $(this).next('.dropdown-menu').toggle();
    });

    const inputs = $('input');
    for (const input of inputs) {
        ValidatorModule.validateRequired(input);
    }

    $('#age').on('blur change', function (e) {
        ValidatorModule.validateRange(this, 18, 99);
    });

    $('#gender').on('blur click change', function (e) {
        ValidatorModule.validateRadios(this);
    });

    $('button[type="submit"]').click(function () {
        $('form').on('submit', function (e) {
            e.preventDefault();
        });
        const inputs = $('input');
        const values = $('form').serializeArray();
        const result = ValidatorModule.validateOnSubmit(inputs);
        if (result  === true) {
            $.post( "person", values, function() {
                $("#form .jumbotron .container").html('<h2 class="text-center text-uppercase">Successful Send</h2>');
            }).fail(function () {
                $("#form .jumbotron .container").html('<h2 class="text-center text-danger text-uppercase">Send Error</h2>');
            });
        }

    });
    // const btn = $('button[type="submit"]');
    // console.log(range, overall, req);
    // if (!req && !range && !overall) {
    //     $(btn).attr('disabled', true)
    // } else {
    //     $(btn).removeAttr('disabled')
    // }
})();
